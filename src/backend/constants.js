module.exports.STATIC_PATH           = "../static";
module.exports.DEFAULT_HTTP_PORT     = 3000;
module.exports.PRODUCTION_HTTP_PORT  = 80;
module.exports.PRODUCTION_HTTPS_PORT = 443;