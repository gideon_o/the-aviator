import   path                from 'path';
import   express             from 'express';
import   http                from 'http';
import   https               from 'https'
import { key, cert    }      from '../security/ssl';
import { DEFAULT_HTTP_PORT } from '../constants';
import { STATIC_PATH }       from '../constants';

const app     = express();
const server  = http.Server(app);

app.use(
  express.static(
    path.join(`${__dirname}`, STATIC_PATH)
  )
);

function ssl() {
  const sslServer = https.createServer({key, cert}, app);
  return {
    start: (port = DEFAULT_HTTP_PORT) => sslServer.listen(port, () => console.log(`PORT: ${port}`))
  }
}

function start(port = DEFAULT_HTTP_PORT) {
  server.listen(port, () => console.log(`PORT: ${port}`));
}

export default { start, ssl }



