import { readFileSync } from 'fs'
import   path           from 'path'

const certificate  = path.join(__dirname, './localhost.crt');
const private_key  = path.join(__dirname, './localhost.key');

export const cert = readFileSync(certificate);
export const key  = readFileSync(private_key);