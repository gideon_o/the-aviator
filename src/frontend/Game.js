import * as THREE from 'three';
import createScene from './utility/scene';
import { createHemisphereLight, createShadowLight } from './light/HemisphereLight';
import createSea from './component/Sea'
import createSky from './component/Sky'
import createAirPlane from './component/AirPlane'
import createPilot from './component/Pilot'
import initMouseControl, { currentPosition as currentMousePosition } from './control/mouse-control'

export default () => {
    // set up the scene, the camera and the renderer
    const { scene, camera, renderer } = createScene();
    const hemisphereLight = createHemisphereLight();
    const shadowLight = createShadowLight();
    const { sea, moveWaves } = createSea();
    const sky = createSky();
    const { plane, propeller, updatePlanePosition } = createAirPlane();
    const { pilot, moveHairs } = createPilot();

    plane.add(pilot);

    scene.add(hemisphereLight);
    scene.add(shadowLight);
    scene.add(sea);
    scene.add(sky);
    scene.add(plane);
    scene.add(new THREE.AmbientLight(0xdc8874, .5));

    initMouseControl();

    function loop(){
        // Rotate the propeller, the sea and the sky
        propeller.rotation.x += 0.3;
        sea.rotation.z += .005;
        sky.rotation.z += .01;

        updatePlanePosition(currentMousePosition());
        moveHairs();
        moveWaves();

        // render the scene
        renderer.render(scene, camera);

        // call the loop function again
        requestAnimationFrame(loop);
    }

    loop();

}