import * as THREE from 'three';
import { white } from '../constants';

const Cloud = () => {
    const mesh = new THREE.Object3D();
    const geom = new THREE.BoxGeometry(20, 20, 20);
    const mat = new THREE.MeshPhongMaterial({color: white});

    // duplicate the geometry a random number of times
    const nBlocks = 3 + Math.floor(Math.random()*3);

    for (let i = 0; i < nBlocks; i++) {
        let m = new THREE.Mesh(geom, mat);
        m.position.x = i*15;
        m.position.y = Math.random()*10;
        m.position.z = Math.random()*10;
        m.rotation.z = Math.random()*Math.PI*2;
        m.rotation.y = Math.random()*Math.PI*2;
        let s = .1 + Math.random()*.9;
        m.scale.set(s,s,s);
        m.castShadow = true;
        m.receiveShadow = true;
        mesh.add(m);
    }
    return mesh;
};

export default () => Cloud();