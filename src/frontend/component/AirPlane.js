import * as THREE from 'three';
import { brown, brownDark, red, white } from '../constants'

const normalize = (v,vmin,vmax,tmin, tmax) => {
    const nv = Math.max(Math.min(v,vmax), vmin);
    const dv = vmax - vmin;
    const pc = (nv - vmin) / dv;
    const dt = tmax - tmin;
    return  tmin + (pc * dt);
};

const AirPlane = () => {
    const mesh = new THREE.Object3D();

    // Create the cabin
    const geomCockpit = new THREE.BoxGeometry(60,50,50,1,1,1);
    const matCockpit = new THREE.MeshPhongMaterial({color: red, flatShading: THREE.FlatShading});
    const cockpit = new THREE.Mesh(geomCockpit, matCockpit);
    cockpit.castShadow = true;
    cockpit.receiveShadow = true;
    mesh.add(cockpit);

    // Create the engine
    const geomEngine = new THREE.BoxGeometry(20,50,50,1,1,1);
    const matEngine = new THREE.MeshPhongMaterial({color: white, flatShading:THREE.FlatShading});
    const engine = new THREE.Mesh(geomEngine, matEngine);
    engine.position.x = 40;
    engine.castShadow = true;
    engine.receiveShadow = true;
    mesh.add(engine);

    // Create the tail
    const geomTailPlane = new THREE.BoxGeometry(15,20,5,1,1,1);
    const matTailPlane = new THREE.MeshPhongMaterial({color: red, flatShading:THREE.FlatShading});
    const tailPlane = new THREE.Mesh(geomTailPlane, matTailPlane);
    tailPlane.position.set(-35,25,0);
    tailPlane.castShadow = true;
    tailPlane.receiveShadow = true;
    mesh.add(tailPlane);

    // Create the wing
    const geomSideWing = new THREE.BoxGeometry(40,8,150,1,1,1);
    const matSideWing = new THREE.MeshPhongMaterial({color: red, flatShading:THREE.FlatShading});
    const sideWing = new THREE.Mesh(geomSideWing, matSideWing);
    sideWing.castShadow = true;
    sideWing.receiveShadow = true;
    mesh.add(sideWing);

    // propeller
    const geomPropeller = new THREE.BoxGeometry(20,10,10,1,1,1);
    const matPropeller = new THREE.MeshPhongMaterial({color: brown, flatShading:THREE.FlatShading});
    const propeller = new THREE.Mesh(geomPropeller, matPropeller);
    propeller.castShadow = true;
    propeller.receiveShadow = true;

    // blades
    const geomBlade = new THREE.BoxGeometry(1,100,20,1,1,1);
    const matBlade = new THREE.MeshPhongMaterial({color: brownDark, flatShading:THREE.FlatShading});

    const blade = new THREE.Mesh(geomBlade, matBlade);
    blade.position.set(8,0,0);
    blade.castShadow = true;
    blade.receiveShadow = true;
    propeller.add(blade);
    propeller.position.set(50,0,0);
    mesh.add(propeller);

    mesh.scale.set(.25,.25,.25);
    mesh.position.y = 100;

    const updatePlanePosition = ({ x, y }) => {
        mesh.position.y = normalize(y, -.75,.75,25, 175);
        mesh.position.x = normalize(x, -.75,.75,-100, 100);

        mesh.position.y += (y - mesh.position.y) * 0.1;
        // mesh.rotation.z = (y - mesh.position.y) * 0.0128;
        // mesh.rotation.x = (mesh.position.y - y) * 0.0064;

        propeller.rotation.x += 0.3;
    };

    return { plane: mesh, propeller, updatePlanePosition };
};

export default () => AirPlane();