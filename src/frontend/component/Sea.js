import * as THREE from 'three';
import { blue } from '../constants'

const Waves = (sea, waves) => {
    sea.mergeVertices();

    const l = sea.vertices.length;

    for (let i = 0; i < l; i++) {
        // get each vertex
        let v = sea.vertices[i];

        // store some data associated to it
        waves.push({y:v.y,
            x:v.x,
            z:v.z,
            // a random angle
            ang:Math.random()*Math.PI*2,
            // a random distance
            amp:5 + Math.random()*15,
            // a random speed between 0.016 and 0.048 radians / frame
            speed:0.016 + Math.random()*0.032
        });
    }
};

const Sea = () => {
    const geom = new THREE.CylinderGeometry(600,600,800,40,10);

    geom.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI/2));

    let waves = [];

    //side effect
    Waves(geom, waves);

    // create the material
    const mat = new THREE.MeshPhongMaterial({
        color: blue,
        transparent: true,
        opacity: 0.6,
        flatShading: THREE.FlatShading,
    });

    const mesh = new THREE.Mesh(geom, mat);
    mesh.receiveShadow = true;
    mesh.position.y = -600;

    const moveWaves = () => {
        const vertices = mesh.geometry.vertices;
        const l = vertices.length;

        for (let i = 0; i < l; i++) {
            let v = vertices[i];

            // get the data associated to it
            let vprops = waves[i];

            // update the position of the vertex
            v.x = vprops.x + Math.cos(vprops.ang) * vprops.amp;
            v.y = vprops.y + Math.sin(vprops.ang) * vprops.amp;

            // increment the angle for the next frame
            vprops.ang += vprops.speed;
        }

        mesh.geometry.verticesNeedUpdate=true;
        mesh.rotation.z += .005;
    };

    return { sea: mesh, moveWaves };
};

export default () => Sea();

