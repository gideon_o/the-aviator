import * as THREE from 'three';
import Cloud from './Cloud';

const Sky = () => {
    const mesh = new THREE.Object3D();
    const nClouds = 20;
    const stepAngle = Math.PI * 2 / nClouds;

    for(let i=0; i< nClouds; i++){
        let c = Cloud();

        let a = stepAngle * i;
        let h = 750 + Math.random()*200;

        c.position.y = Math.sin(a)*h;
        c.position.x = Math.cos(a)*h;
        c.rotation.z = a + Math.PI/2;
        c.position.z = -400 - Math.random() * 400;

        let s = 1 + Math.random() * 2;
        c.scale.set(s,s,s);

        mesh.add(c);
    }
    mesh.position.y = -600;
    return mesh
};

export default () => Sky();