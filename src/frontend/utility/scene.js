import * as THREE     from 'three';
import * as Constants from '../constants';

const renderCanvas = (renderer) => {
    document.body.insertBefore(
        renderer.domElement,
        document.querySelector('script')
    );
};

const handleWindowResize = (renderer, camera) => {
    Constants.HEIGHT = window.innerHeight;
    Constants.WIDTH = window.innerWidth;
    renderer.setSize(Constants.WIDTH, Constants.HEIGHT);
    camera.aspect = Constants.WIDTH / Constants.HEIGHT;
    camera.updateProjectionMatrix();
};

export default () => {
    const scene = new THREE.Scene();
    scene.fog = new THREE.Fog(0xf7d9aa, 100, 950);

    const renderer = createRenderer();
    const camera = createCamera();
    renderCanvas(renderer);
    handleWindowResize(renderer, camera);
    return { scene, camera, renderer };
};

export const createCamera = () => {
    const camera = new THREE.PerspectiveCamera(
        Constants.fieldOfView,
        Constants.aspectRatio,
        Constants.nearPlane,
        Constants.farPlane
    );

    camera.position.x = Constants.camera_position_x;
    camera.position.z = Constants.camera_position_z;
    camera.position.y = Constants.camera_position_y;
    return camera;
};

export const createRenderer = () => {
    const renderer = new THREE.WebGLRenderer({
        alpha: true,
        antialias: true
    });

    renderer.setSize(Constants.WIDTH, Constants.HEIGHT);
    renderer.shadowMap.enabled = true;

    return renderer;
};
