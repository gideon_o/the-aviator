import * as THREE from 'three';
import * as Constants from '../constants'

export const createShadowLight = () => {
    const shadowLight = new THREE.DirectionalLight(0xffffff, .9);
    shadowLight.position.set(150, 350, 350);
    shadowLight.castShadow = true;

    shadowLight.shadow.camera.left = Constants.shadowLight_direction_left;
    shadowLight.shadow.camera.right = Constants.shadowLight_direction_right;
    shadowLight.shadow.camera.top = Constants.shadowLight_direction_top;
    shadowLight.shadow.camera.bottom = Constants.shadowLight_direction_bottom;
    shadowLight.shadow.camera.near = Constants.shadowLight_direction_near;
    shadowLight.shadow.camera.far = Constants.shadowLight_direction_far;

    shadowLight.shadow.mapSize.width = Constants.shadowLight_mapSize_width;
    shadowLight.shadow.mapSize.height = Constants.shadowLight_mapSize_height;
    return shadowLight
};

export const createHemisphereLight = () => {
    return  new THREE.HemisphereLight(0xaaaaaa,0x000000, .9)
};