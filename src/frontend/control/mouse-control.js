import { HEIGHT, WIDTH } from '../constants';
let position = { x: 0, y: 0};

const handleMouseMove = ({clientX, clientY}) => {
    position = {
        x: -1 + (clientX / WIDTH) * 2,
        y: 1 - (clientY / HEIGHT) * 2
    };
};

export default () => document.addEventListener('mousemove', handleMouseMove, false);
export const currentPosition = () => position;
