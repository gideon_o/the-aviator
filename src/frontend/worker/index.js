// export worker api
import Worker from "worker-loader?name=hash.worker.js!./task";
const worker = new Worker;

export default () => {
//method signature worker.postMessage(message, [ArrayBuffer | MessagePort | ImageBitmap]);
    worker.postMessage("message", );
    worker.onmessage = (e) => console.log(e.data); //message received
}