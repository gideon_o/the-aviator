 module.exports.red = 0xf25346;
 module.exports.white = 0xd8d0d1;
 module.exports.brown = 0x59332e;
 module.exports.pink = 0xF5986E;
 module.exports.brownDark = 0x23190f;
 module.exports.blue = 0x68c3c0;

 module.exports.WIDTH = window.innerWidth;
 module.exports.HEIGHT = window.innerHeight;
 module.exports.aspectRatio = window.innerWidth / window.innerHeight;
 module.exports.fieldOfView = 60;
 module.exports.nearPlane = 1;
 module.exports.farPlane = 10000;

 module.exports.camera_position_x = 0;
 module.exports.camera_position_z = 200;
 module.exports.camera_position_y = 100;

 module.exports.shadowLight_direction_left = -400;
 module.exports.shadowLight_direction_right = 400;
 module.exports.shadowLight_direction_top = 400;
 module.exports.shadowLight_direction_bottom = -400;
 module.exports.shadowLight_direction_near = 1;
 module.exports.shadowLight_direction_far = 1000;
 module.exports.shadowLight_mapSize_width = 2048;
 module.exports.shadowLight_mapSize_height = 2048;
