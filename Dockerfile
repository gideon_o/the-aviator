FROM node:11-alpine
#FROM node:latest
WORKDIR /application
COPY package.json /application

RUN npm install --loglevel verbose
RUN apk add --no-cache bash
RUN apk add curl

COPY . /application
RUN npm run build
EXPOSE 3000
CMD ["npm", "start"]