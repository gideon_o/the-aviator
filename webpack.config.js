const path              = require('path');
const webpack           = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const DEFAULT_HTTP_PORT = require('./src/backend/constants').DEFAULT_HTTP_PORT;

const SRC               = path.join(__dirname, './src');
const ENTRY             = path.join(SRC, './frontend/index.js');
const STATIC            = path.join(SRC, './backend/dist/static');
const OUTPUT            = Object.assign({ path: STATIC, filename: 'bundle.[hash].js' });

const isProd = process.env.NODE_ENV === 'production';

const rules = () => [
  {
    test: /\.js$/,
    exclude: [/node_modules/],
    loader: "babel-loader",
  }
];

const defaultPlugins = () => {
  return [
    new HtmlWebpackPlugin(Object.assign({
      template: path.join(SRC, './frontend/index.html'),
      filename: 'index.html'}), isProd && ({
      minify: {
        collapseWhitespace: true,
        collapseInlineTagWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true
      }}))
  ,  new webpack.DefinePlugin({
      IS_PRODUCTION: isProd
    })
  , new webpack.LoaderOptionsPlugin({
      options: {
        worker: {
          output: {
            filename: "task.js",
            chunkFilename: "[id].hash.worker.js"
          }
        }
      }
    })
  ]
};

function plugins() {
  return defaultPlugins();
}

function devServer() {
  return {
    port: DEFAULT_HTTP_PORT
    , historyApiFallback: true
    , hot: true
    , contentBase: STATIC
    , inline: true
    , progress: true
    , open: false
  }
}

module.exports = {
  entry: ENTRY
, output: OUTPUT
, module: { rules: rules() }
, plugins: plugins()
, devServer: devServer()
};